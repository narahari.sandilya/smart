#!/usr/bin/env ruby

require_relative '../log_parser'

file_path = ARGV[0]
LogParser.new(from_file: file_path).show_stats