# frozen_string_literal: true

models = File.expand_path('models/', __dir__)
$LOAD_PATH.unshift(models)

require 'page'
require 'page_collection'

class LogParser
  attr_reader :file_data, :from_file

  def initialize(from_file:)
    @from_file = from_file
    @file_data = load_file_data
  end

  def show_stats
    File.open('output.txt', 'w') do |f|
      f.write "List of webpages with most page views ordered from most pages views to less page views\n\n"
      pages.order_pages_by_total_views.each do |page|
        f.write "#{page.uri} #{page.total_views} views\n"
      end
      f.write "\n"
      f.write "List of webpages with most unique page views also ordered\n\n"
      pages.order_pages_by_unique_views.each do |page|
        f.write "#{page.uri} #{page.unique_views} views\n"
      end
    end
  end

  private

  def load_file_data
    file = File.open(from_file)
    data = file.readlines.map(&:chomp)
    file.close
    data
  end

  def pages
    page_collection ||= PageCollection.new(log_records: file_data)
    page_collection
  end
end
