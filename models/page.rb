# frozen_string_literal: true

class Page
  attr_reader :uri, :visited_by

  def initialize(uri:)
    @uri = uri
    @visited_by ||= []
  end

  def visited_by=(ip_address)
    @visited_by << ip_address
  end

  def total_views
    @visited_by.count
  end

  def unique_views
    @visited_by.uniq.count
  end
end
