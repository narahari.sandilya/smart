# frozen_string_literal: true

class PageCollection
  attr_reader :visited_pages, :list_of_page_objects

  def initialize(log_records:)
    @visited_pages = log_records
    @list_of_page_objects = build_pages
  end

  def build_pages
    list_of_uri = {}

    visited_pages.map do |log_line|
      uri, ip_address = log_line.split(' ')

      if list_of_uri.key?(uri)
        page = list_of_uri[uri]
        page.visited_by = ip_address
        next
      end

      page = Page.new(uri: uri)
      page.visited_by = ip_address
      list_of_uri[uri] = page
      page
    end.compact
  end

  def order_pages_by_total_views
    list_of_page_objects.sort_by(&:total_views).reverse
  end

  def order_pages_by_unique_views
    list_of_page_objects.sort_by(&:unique_views).reverse
  end
end
