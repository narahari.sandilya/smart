# Smart take home test

## Requirements:

* Ruby >= 3.0.0

## Setup:
    gem install bundler
    bundle install

## To Run:

    ruby bin/start_parse.rb webserver.log

## To Test:

    bundle exec rspec

## Refactor:

* Hanlde negative paths
* Add logging
