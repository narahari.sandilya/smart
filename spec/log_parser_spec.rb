# frozen_string_literal: true

require_relative '../log_parser'

RSpec.describe 'LogParser' do
  let(:subject) { LogParser.new(from_file: file_path) }

  let(:file_data) do
    [
      '/foo/1 1.2.3.4'
    ]
  end

  let(:file_path) { 'dummy_path' }
  let(:file) { double('File', readlines: file_data, close: {}) }

  before do
    allow(File).to receive(:open).and_return(file)
  end

  context '#intialize' do
    it 'sets the from_file' do
      expect(subject.from_file).to eq(file_path)
    end

    it 'sets the file_data' do
      expect(subject.file_data).to eq(file_data)
    end
  end

  context '#show_stats' do
    let(:pages) do
      [
        page1
      ]
    end

    let(:page_collection) do
      instance_double('PageCollection',
                      order_pages_by_total_views: pages,
                      order_pages_by_unique_views: pages)
    end

    let(:page1) do
      instance_double('Page',
                      uri: 'foo/1',
                      total_views: 1)
    end

    let(:header_text_total_views) do
      "List of webpages with most page views ordered from most pages views to less page views\n\n"
    end

    let(:header_text_unique_views) do
      "List of webpages with most unique page views also ordered\n\n"
    end

    let(:page1_response) do
      "/foo/1 1 views\n"
    end

    it 'writes the guest_list customers in the file' do
      expect(File).to receive(:open).with('output.txt', 'w').and_yield(file)
      expect(file).to receive(:write).with(header_text_total_views)
      expect(file).to receive(:write).with(page1_response).at_least(:once)
      expect(file).to receive(:write).with("\n")
      expect(file).to receive(:write).with(header_text_unique_views)
      subject.show_stats
    end
  end
end
