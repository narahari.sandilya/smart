# frozen_string_literal: true

require_relative '../../models/page_collection'
require_relative '../../models/page'

RSpec.describe 'PageCollection' do
  let(:subject) { PageCollection.new(log_records: log_records) }

  let(:log_records) { ['/foo/1 1.2.3.4', '/bar/1 2.3.4.5'] }

  it 'sets visited_pages' do
    expect(subject.visited_pages).to eq(log_records)
  end

  context 'sets list_of_page_objects' do
    context 'when same page is visited multiple times' do
      let(:log_records) { ['/foo/1 1.2.3.4', '/foo/1 2.3.4.5'] }

      it 'returns a list containing only one page object' do
        expect(subject.list_of_page_objects)
          .to match_array([
                            have_attributes(
                              uri: '/foo/1',
                              visited_by: ['1.2.3.4', '2.3.4.5']
                            )
                          ])
      end
    end

    context 'when multiples pages are visited multiple times' do
      let(:log_records) do
        ['/foo/1 1.2.3.4', '/foo/1 2.3.4.5',
         '/bar/1 1.2.3.4', '/bar/1 2.3.4.5']
      end

      it 'returns a list containing two page objects' do
        expect(subject.list_of_page_objects)
          .to match_array([
                            have_attributes(
                              uri: '/foo/1',
                              visited_by: ['1.2.3.4', '2.3.4.5']
                            ),
                            have_attributes(
                              uri: '/bar/1',
                              visited_by: ['1.2.3.4', '2.3.4.5']
                            )
                          ])
      end
    end

    context 'when multiples pages are visited' do
      let(:log_records) do
        ['/foo/1 1.2.3.4',
         '/foo/2 2.3.4.5']
      end

      it 'returns a list containing two page objects' do
        expect(subject.list_of_page_objects)
          .to match_array([
                            have_attributes(
                              uri: '/foo/1',
                              visited_by: ['1.2.3.4']
                            ),
                            have_attributes(
                              uri: '/foo/2',
                              visited_by: ['2.3.4.5']
                            )
                          ])
      end
    end
  end

  context '#order_pages_by_total_views' do
    let(:log_records) do
      ['/foo/1 1.2.3.4', '/foo/1 2.3.4.5',
       '/foo/2 1.2.3.4', '/foo/2 2.3.4.5', '/foo/2 2.3.4.5']
    end

    it 'first object in the array has the most views' do
      expect(subject.order_pages_by_total_views.first.uri)
        .to eq(
          '/foo/2'
        )
    end
  end

  context '#order_pages_by_unique_views' do
    let(:log_records) do
      ['/foo/1 1.2.3.4', '/foo/1 2.3.4.5',
       '/foo/2 2.3.4.5', '/foo/2 2.3.4.5', '/foo/2 2.3.4.5']
    end

    it 'first object in the array has the most unique views' do
      expect(subject.order_pages_by_unique_views.first.uri)
        .to eq(
          '/foo/1'
        )
    end
  end
end
