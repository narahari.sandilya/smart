# frozen_string_literal: true

require_relative '../../models/page'

RSpec.describe 'Page' do
  let(:subject) { Page.new(uri: uri) }

  let(:uri) { '/foor/1' }

  it 'sets the name' do
    expect(subject.uri).to eq('/foor/1')
  end

  context 'when visited' do
    context 'once' do
      before { subject.visited_by = '1.2.3.4' }

      it 'sets the visited_by' do
        expect(subject.visited_by).to eq(['1.2.3.4'])
      end

      it 'sets total_views to 1' do
        expect(subject.total_views).to eq(1)
      end

      it 'sets the unique_views to 1' do
        expect(subject.unique_views).to eq(1)
      end
    end

    context 'multiple times' do
      before do
        subject.visited_by = '1.2.3.4'
        subject.visited_by = '1.2.3.4'
        subject.visited_by = '2.3.4.5'
      end

      it 'sets the visited_by' do
        expect(subject.visited_by).to eq(['1.2.3.4', '1.2.3.4', '2.3.4.5'])
      end

      it 'sets total_views to 1' do
        expect(subject.total_views).to eq(3)
      end

      it 'sets the unique_views to 1' do
        expect(subject.unique_views).to eq(2)
      end
    end
  end
end
